package com.javarush.task.task22.task2209;

import java.util.ArrayList;

public class Graph {
    Vertex[] vertex;
    int[][] matrix;

    public Graph(String[] words) {
        matrix = new int[words.length][words.length];
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words.length; j++) {
                char startChar = words[j].toLowerCase().charAt(0);
                char endChar = words[i].toLowerCase().charAt(words[i].length() - 1);
                if (startChar == endChar && !words[i].equals(words[j])) {
                    matrix[i][j] = 1;
                } else matrix[i][j] = 0;
            }
        }
        vertex = new Vertex[words.length];
        for (int i = 0; i < words.length; i++) {
            vertex[i] = new Vertex(words[i], i);
        }
    }

    public ArrayList<Branch> setBranches(Branch branch) throws CloneNotSupportedException {
        ArrayList<Branch> branches = new ArrayList<>();
        int index = 0;
        int x = branch.vertex.size();
        index = branch.vertex.get(x - 1).index;

        for (int q = 0; q < matrix.length; q++) {
            if (matrix[index][q] == 1 && !cheched(branch, vertex[q])) {
                Branch copy = branch.clone();
                copy.vertex.add(vertex[q]);
                branches.add(copy);
            }
        }
        return branches;
    }

    public boolean cheched(Branch branch, Vertex vertex) {
        String value = vertex.value;
        for (int i = 0; i < branch.vertex.size(); i++) {
            if (branch.vertex.get(i).value.equals(value))
                return true;
        }
        return false;
    }

    public void sOut() {

        for (int i = 0; i < matrix.length; i++) {
            System.out.println();
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
    }
}
