package com.javarush.task.task22.task2209;

import java.util.ArrayList;

public class Tree {
    ArrayList<Branch> branches = new ArrayList<>();
    String root;

    public Tree(String root) {
        this.root = root;
    }

    public ArrayList<String> createWords(Graph graph) throws CloneNotSupportedException {
        ArrayList<Branch> createBranches;
        StringBuilder word = new StringBuilder();
        ArrayList<String> words = new ArrayList<>();
        for (int i = 0; i < graph.vertex.length; i++) {
            if (graph.vertex[i].value.equals(root)) {
                branches.add(new Branch(new Vertex(root, i)));
            }
        }
        while (branches.size() != 0) {
            createBranches = graph.setBranches(branches.get(0));
            if (createBranches.size() == 0) {
                for (Vertex vertex : branches.get(0).vertex) {
                    word.append(vertex.value).append(" ");
                }
                words.add(word.toString().trim());
                word = new StringBuilder();
            }
            branches.remove(0);
            branches.addAll(createBranches);
        }

        return words;

    }

}
