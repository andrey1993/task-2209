package com.javarush.task.task22.task2209;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/*
Составить цепочку слов
*/
public class Solution {
    public static void main(String[] args) {
        //...
        BufferedReader readConsole = new BufferedReader(new InputStreamReader(System.in));
        try {
            BufferedReader readFile = new BufferedReader(new FileReader(readConsole.readLine()));
            readConsole.close();
     //       BufferedReader readFile = new BufferedReader(new FileReader("D://gg.txt"));
            String words[] = null;
            String string;
            while ((string = readFile.readLine()) != null) {
                string = string.replaceAll("\uFEFF", "");
                string = string.trim();
                words = string.split(" ");
            }
            readFile.close();
            StringBuilder result = getLine(words);
            System.out.println(result.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static StringBuilder getLine(String... words) throws CloneNotSupportedException {
        if (words.length == 0) return new StringBuilder();
        StringBuilder result = new StringBuilder();
        ArrayList<String> words2 = new ArrayList<>();
        Graph graph1 = new Graph(words);
        for (String word : words) {
            Tree tree = new Tree(word);
            words2.addAll(tree.createWords(graph1));
        }
        Collections.sort(words2);
        int x = 0;
        for (int i = 0; i < words2.size(); i++) {
            if (words2.get(i).length() > x) {
                x = words2.get(i).length();
                result = new StringBuilder();
                result.append(words2.get(i));
            }
        }
//        for (String str : words) {
//            if (!result.toString().contains(str)) {
//                result.append(" ").append(str);
//            }
//        }
        return result;
    }
}
