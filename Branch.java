package com.javarush.task.task22.task2209;

import java.util.ArrayList;

public class Branch implements Cloneable {
    ArrayList<Vertex> vertex = new ArrayList<>();

    public Branch(Vertex vertex) {
        this.vertex.add(vertex);
    }

    protected Branch clone() throws CloneNotSupportedException {
        Branch branchClone = new Branch(vertex.get(0));
        for (int i = 1; i < vertex.size();i ++) {
            branchClone.vertex.add(vertex.get(i));
        }
        return branchClone;
    }

}
