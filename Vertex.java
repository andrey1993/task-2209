package com.javarush.task.task22.task2209;

public class Vertex {
    String value;
    int index;

    public Vertex(String value, int index) {
        this.value = value;
        this.index = index;
    }

    public String getValue() {
        return value;
    }
}
